# Contributor: Iztok Fister, Jr. <iztok@iztok-jr-fister.eu>
# Maintainer: Iztok Fister, Jr. <iztok@iztok-jr-fister.eu>
pkgname=py3-hiplot
pkgver=0.1.32
pkgrel=0
pkgdesc="HiPlot makes understanding high dimensional data easy"
url="https://github.com/facebookresearch/hiplot"
arch="noarch"
license="MIT"
depends="
	python3
	ipython
	py3-beautifulsoup4
	py3-flask
	py3-flask-compress
	py3-pandas
	"
makedepends="py3-gpep517 py3-setuptools py3-wheel"
checkdepends="py3-pytest-xdist py3-pytest-cov py3-ipykernel py3-optuna"
subpackages="$pkgname-pyc"
source="https://github.com/facebookresearch/hiplot/archive/$pkgver/hiplot-$pkgver.tar.gz"
builddir="$srcdir/hiplot-$pkgver"

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {

	# assertion errors or missing dependencies
	local _skipped_tests="
		test_to_filename
		and not test_to_html
		and not test_demos_streamlit
		and not test_demos_ipython
		"

	python3 -m venv --clear --without-pip --system-site-packages .testenv
	.testenv/bin/python3 -m installer .dist/*.whl
	.testenv/bin/python3 -m pytest -n auto -k "not $(echo $_skipped_tests)"
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/*.whl
}

sha512sums="
df5bf7f21fd4676b452480a7ff301800a050660e6403bdba11dd3f8fcd5f6ccff0e3123bfcc636b72e26fe6e984d284664434761b9bec9056e0d1520f4790e95  hiplot-0.1.32.tar.gz
"
